<?php

if (!function_exists('dd')) {
    function dd($var, $depth = 10, $highlight = true)
    {
        yii\helpers\VarDumper::dump($var, $depth, $highlight);
        die(1);
    }
}


if (!function_exists('h')) {
    function h($text)
    {

        return yii\helpers\Html::encode($text);
    }
}