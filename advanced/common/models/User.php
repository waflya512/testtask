<?php
namespace common\models;

use Yii;
use common\models\Subscription;
use common\models\UserSubscription;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $login
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

//    public $subscribtionDate;

    public function fields()
    {
        return ['id', 'login', 'fio', 'subscriptionDate'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This login has already been taken.'],
            ['login', 'string', 'min' => 2, 'max' => 255],
            ['login',
                'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9]/',
                'message' => 'Invalid characters in login.',
            ],

//            ['subscribtionDate', 'string'],

            ['first_name', 'string', 'min' => 2, 'max' => 30],
            ['second_name', 'string', 'min' => 2, 'max' => 30],
            ['middle_name', 'string', 'min' => 2, 'max' => 30],
            [['login', 'first_name', 'second_name', 'middle_name', 'email'], 'required'],
            ['email', 'email'],

            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    public function getFIO()
    {
        return $this->first_name . ' ' . $this->second_name . " " . $this->middle_name;
    }

    public function getSubscription()
    {
        return $this->hasOne(UserSubscription::className(), ['user_id' => 'id']);
    }

    public function getPassword()
    {
        return "";
    }

    public function getSubscriptionDate()
    {
        if ($this->subscription == null) {
            return "";
        }

        $timestamp = $this->subscription->ends_at;
        return Yii::$app->formatter->asDate($timestamp, 'd-M-Y');
    }

    public function beforeSave($insert)
    {
        $request = Yii::$app->request;
        $params = $request->bodyParams;
        $password = $request->getBodyParam('User')["password"]; //any other way to get subscriptionDate here?

        if ($this->isNewRecord && empty($password)) {
            throw new \InvalidArgumentException("Password is required for new users.");
        }

        if (!empty($password)) {
            $this->setPassword($password);
        }

        $this->generateAuthKey();
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $request = Yii::$app->request;
        $params = $request->bodyParams;
        $subscriptionDate = $request->getBodyParam('User')["subscriptionDate"]; //any other way to get subscriptionDate here?


        if ($subscriptionDate != "") {
            $subscription = $this->subscription != null ? $this->subscription : new UserSubscription();
            $subscription->user_id = $this->id;
            $timestamp = Yii::$app->formatter->asTimestamp($subscriptionDate);
            $subscription->ends_at = $timestamp;
            $subscription->save();
        } else {
            $this->unlinkAll('subscription', true);
        }

        return parent::afterSave($insert, $changedAttributes);
    }
}
