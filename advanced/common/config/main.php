<?php

require_once( dirname(__FILE__) . '/../components/GlobalFunctions.php');

return [
    'timeZone' => 'Europe/Moscow',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ]
    ],
];
