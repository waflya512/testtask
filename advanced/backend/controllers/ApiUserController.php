<?php

namespace backend\controllers;

use yii\rest\ActiveController;

class ApiUserController extends ActiveController
{
    public $modelClass = 'common\models\User';
}
