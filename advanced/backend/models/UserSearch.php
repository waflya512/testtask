<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{

    public $fio;
    public $subscribeLastDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['login', 'email', 'fio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->with('subscription');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->setSort([
            'attributes' => [
                'id',
                'login',
                'fio' => [
                    'asc' => ['first_name' => SORT_ASC, 'second_name' => SORT_ASC, 'middle_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'second_name' => SORT_DESC, 'middle_name' => SORT_DESC],
                    'default' => SORT_ASC
                ],

                'email',
//                'subscribtionDate' => [
//                    'asc' => ['user_subscription.ends_at' => SORT_ASC],
//                    'desc' => ['user_subscription.ends_at' => SORT_DESC],
//                ] // how to sort by subscription date?

            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andWhere('first_name LIKE "%' . $this->fio . '%" ' .
                'OR second_name LIKE "%' . $this->fio . '%"' .
                'OR middle_name LIKE "%' . $this->fio . '%"'
            )
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
