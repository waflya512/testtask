<?php

use yii\db\Migration;

class m170305_080040_create_testtask_tables extends Migration
{
    public function up()
    {
        $this->dropColumn('user', 'username');
        $this->addColumn('user', 'login', $this->string()->notNull()->unique());
        $this->addColumn('user', 'first_name', $this->string(30)->notNull());
        $this->addColumn('user', 'second_name', $this->string(30)->notNull());
        $this->addColumn('user', 'middle_name', $this->string(30)->notNull());

        $this->createTable('user_subscription', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'text' => $this->string(),
            'ends_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-user_subscription-user_id',
            'user_subscription',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_subscription-user_id',
            'user_subscription',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-user_subscription-user_id',
            'user_subscription'
        );

        $this->dropIndex(
            'idx-user_subscription-user_id',
            'user_subscription'
        );

        $this->dropTable('user_subscription');
        $this->dropColumn('user', 'login');
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'second_name');
        $this->dropColumn('user', 'middle_name');
        $this->addColumn('user', 'username', $this->string()->notNull()->unique());
    }


}
