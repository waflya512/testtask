<?php

namespace console\controllers;

use common\models\UserSubscription;
use Yii;
use yii\console\Controller;
use common\models\User;

class SubscriptionController extends Controller
{
    public $message;

    public function options($actionID)
    {
        return ['message'];
    }

    public function optionAliases()
    {
        return ['m' => 'message'];
    }

    public function actionIndex()
    {
    }

    public function actionClear()
    {
        $timestamp = Yii::$app->formatter->asTimestamp(date("Y-m-d H:i:s"));
        UserSubscription::deleteAll(['<', "ends_at", $timestamp]);

        echo "done!";
    }
}